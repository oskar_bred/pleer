<?php
session_start();

if(isset($_GET['id'])){
	$id=$_GET['id'];
	$url="http://api.pleer.com/index.php";
	$token=$_SESSION['token'];
	$ch=curl_init($url);
	curl_setopt_array($ch, [
		CURLOPT_POST => true,
		CURLOPT_POSTFIELDS => [
			'method' => 'tracks_get_download_link',
			'track_id' => $id,
			'reason' => 'listen',
		],
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_HTTPHEADER => ['Authorization: Bearer ' . $token,] ,
	]);
	$song = curl_exec($ch);
	$song = json_decode($song, true);
	?>
	<audio src='<?php echo $song["url"] ?>' controls> <?php
}
